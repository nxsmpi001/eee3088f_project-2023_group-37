### EEE3088F Project 2023 - Group 37

## What is this repo for?
EEE3088F is 3rd year electrical design course taught at the University of Cape Town. The main project for this year is to design a HAT(Hardware Attached on Top) that will log data from an STM32 target board. The purpose of this repository is to store all the documentation, code and CAD files for our project submission and allow our team to keep track of all the changes made in these files throughout the project.

## Who is it for?
This repo is intended for the staff of the electrical department, specifically the lecturers and course converer running the course as a way to have easy access to our entire project submission, along with a log of each student's contribution.

## How do you install its contents?
You can download this repo to your machine by cloning it by using the following command in the terminal:
```
git clone https://gitlab.com/nxsmpi001/eee3088f_project-2023_group-37
```

## What are the future plans for this repo?
As of now this repo is mostly empty since the project is still in the proposal stage but as the project runs its course, documentation and code concerning each stage of the project will uploaded to this repo.